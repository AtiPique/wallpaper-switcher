pub fn reverse_str(st: String) -> String {
	let mut reversed_str: String = "".to_string();
	let my_str: Vec<char> = st.chars().collect();

	for i in (0..my_str.len()).rev() {
		reversed_str = reversed_str + &my_str[i].to_string();
	}
	
	return reversed_str;
}