use std::fs;
use std::env;

mod str_reverse;

fn main() {
    let mut args: Vec<String> = env::args().collect();
    args.remove(0);

    let dir: String = args[0].to_string();
    let paths = fs::read_dir(dir.clone()).unwrap();

    let mut imgs_path: Vec<String> = vec![];

    for path in paths {
        imgs_path.push((path.unwrap().path().display()).to_string());
    }

    let mut imgs: Vec<String> = vec![];

    for img_path in imgs_path {
        let path: Vec<char> = img_path.chars().collect();
        let mut result: String = "".to_string();

        let mut ext: String = "".to_string();
        ext = ext + &path[path.len()-3].to_string() + &path[path.len()-2].to_string() + &path[path.len()-1].to_string();

        if ext != "jpg" {
            if ext != "png" {
                if ext != "jpeg" {
                    if ext != "ebp" {
                        continue;
                    }
                }
            }
        }

        for i in (0..path.len()).rev() {
		
            if path[i] == '/' {
                break;
            }
            result = result + &path[i].to_string();				
        }

        
        imgs.push(str_reverse::reverse_str(result));
    }


    println!(".jpg files in the {} directory.", dir);
    for i in 0..imgs.len() {
        println!("{}", imgs[i]);
    }
}